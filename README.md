# Physics Pusher
A code sample of a 2D platformer I wrote. The component PhysicsObject can be added to a gameobject which will then move by sending out raycasts for precise movement. PhysicEntities can move and push other PhysicsObjects.

<img src="/Images/Climb.gif" align="middle">

### RaycastCollision
An abstract MonoBehaviour which requires a Boxcollider2D and a vertical and horizontal ray count. It determines where the raycast origins on the given collider will be.

### PhysicsObject
Contains the Move method which tries to alter the gameobjects position with the given velocity. It sends out several raycasts and checks if it’s able to move freely or it moves the distance to a collision object. It keeps track of the collisions during moving as well.

### PhysicsEntity
Inherits from PhysicsObject and is able to push other PhysicsObjects by overriding OnRaycastHit and adding an extra check.

<img src="/Images/PushChest.gif" align="middle">