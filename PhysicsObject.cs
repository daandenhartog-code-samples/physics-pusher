﻿using UnityEngine;

public class PhysicsObject : RaycastCollision {

    public enum Direction { Left, Right, Bottom, Top };
    public Collisions collisions;

    public LayerMask layerCollisions = 1 << 0;
    public float mass = 1f;

    protected LayerMask layerRaycasts;

    protected virtual void Awake() {
        layerRaycasts = layerCollisions;
    }

    public Vector2 Move(Vector2 velocity) {
        SetRaycastOrigins();
        collisions.Reset();
        
        if (velocity.y != 0f) velocity.y = GetMovedVelocity(velocity.y, velocity.y < 0 ? Direction.Bottom : Direction.Top);
        if (velocity.x != 0f) velocity.x = GetMovedVelocity(velocity.x, velocity.x < 0 ? Direction.Left : Direction.Right);
        
        transform.position += (Vector3)velocity;
        return velocity;
    }

    private float GetMovedVelocity(float velocity, Direction direction) {
        RaycastHit2D hit = GetRaycastBasedOnDirection(Mathf.Abs(velocity), direction);

        if (!hit) {
            // If no hit is detected object is allowed to move freely
            return velocity;
        } else {
            // If a hit is detected collisions are updated
            // In OnRaycastHit it is determined what happens when this object touches a certain type of layerMask
            collisions.SetCollision(direction);
            return OnRaycastHit(hit.collider, Mathf.Abs(hit.distance) - skinWidth, velocity, direction);
        }
    }

    protected virtual float OnRaycastHit(Collider2D collider, float distanceToObject, float velocity, Direction direction) {
        // If an obstacle is hit, return the distance to the obstacle
        return distanceToObject * (direction == Direction.Left || direction == Direction.Bottom ? -1 : 1);
    }

    private RaycastHit2D GetRaycastBasedOnDirection(float rayLength, Direction direction) {
        // Based on direction, rays from certain positions with specific spacing and counts will be cast
        switch (direction) {
            default:
            case Direction.Left: return GetClosestRaycastsHit(verticalRayCount, rayLength, raycastOrigins.leftBottom, raycastOrigins.leftTop, Vector2.left);
            case Direction.Right: return GetClosestRaycastsHit(verticalRayCount, rayLength, raycastOrigins.rightBottom, raycastOrigins.rightTop, Vector2.right);
            case Direction.Bottom: return GetClosestRaycastsHit(horizontalRayCount, rayLength, raycastOrigins.leftBottom, raycastOrigins.rightBottom, Vector2.down);
            case Direction.Top: return GetClosestRaycastsHit(horizontalRayCount, rayLength, raycastOrigins.leftTop, raycastOrigins.rightTop, Vector2.up);
        }
    }

    private RaycastHit2D GetClosestRaycastsHit(int rayCount, float rayLength, Vector2 rayOrigin, Vector2 rayEnd, Vector2 rayDirection) {
        RaycastHit2D hit = new RaycastHit2D();

        // Send out several raycasts and return the closests found hit
        for (int i = 0; i < rayCount; i++) {
            Vector2 rayPosition = Vector2.Lerp(rayOrigin, rayEnd, i / (rayCount - 1f));
            RaycastHit2D rayHit = Physics2D.Raycast(rayPosition, rayDirection, rayLength + skinWidth, layerRaycasts);

            if (rayHit && (!hit || (rayHit.distance < hit.distance))) {
                hit = rayHit;
            }
        }

        return hit;
    }

    public struct Collisions {

        public bool left, right;
        public bool bottom, top;

        public void Reset() {
            left = right = false;
            bottom = top = false;
        }

        public void SetCollision(Direction direction) {
            switch (direction) {
                case Direction.Left: left = true; break;
                case Direction.Right: right = true; break;
                case Direction.Bottom: bottom = true; break;
                case Direction.Top: top = true; break;
            }
        }
    }
}