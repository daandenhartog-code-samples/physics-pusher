﻿using UnityEngine;

public class PhysicsEntity : PhysicsObject {

    public LayerMask layerPushables = 1 << 8;

    protected override void Awake() {
        base.Awake();

        layerRaycasts |= layerPushables;
    }

    protected override float OnRaycastHit(Collider2D collider, float distanceToObject, float velocity, Direction direction) {
        // Try and push other PhysicsObject
        if (1 << collider.gameObject.layer == layerPushables && CanPush(direction)) {
            PhysicsObject other = collider.transform.parent.GetComponent<PhysicsObject>();
            int directionModifier = direction == Direction.Left ? -1 : 1;

            float leftoverVelocity = Mathf.Abs(velocity) - distanceToObject;
            float pushDistance = GetPushDistance(leftoverVelocity, other.mass);
            float pushedDistance = Mathf.Abs(other.Move(Vector2.right * pushDistance * directionModifier).x);

            return (distanceToObject + pushedDistance) * directionModifier;
        }
        
        return base.OnRaycastHit(collider, distanceToObject, velocity, direction);
    }

    private float GetPushDistance(float leftoverVelocity, float massOther) {
        return leftoverVelocity * (mass / (mass + massOther));
    }

    private bool CanPush(Direction direction) {
        // Return true when direction is horizontal and PhysicsEntity is grounded
        return (direction == Direction.Left || direction == Direction.Right) && collisions.bottom;
    }
}